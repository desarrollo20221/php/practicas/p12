<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .salida{
                border: 1px solid black;
                width: 80px;
                height: 40px;
                line-height: 40px;
                text-align: center;
                display: inline-block;
            }
        </style>

    </head>
    <body>
        <?php
            $ancho="200px" ;
            $alto="200px" ;
            
            $dados = [1,2,3,4,5,6];
            
            // esto es para un dado
            $numeroDados = count($dados);
            $indice = mt_rand(0,$numeroDados-1);
            $dado = $dados[$indice];
            
            // esto es para el segundo dado
            $numeroDados1 = count($dados);
            $indice1 = mt_rand(0,$numeroDados1-1);
            $dado1 = $dados[$indice1];
            
            // sumo el resultado de los 2 dados
            $total = $dado+$dado1;
        ?>
        <div style="width: <?=((int) $ancho)*2 ?>px;">
            <img style="width: <?= $ancho ?>; height: <?= $alto ?>; " src="imgs/<?= $dado ?>.svg"/>
            <img style="width: <?= $ancho ?>; height: <?= $alto ?>; " src="imgs/<?= $dado1 ?>.svg"/>
        </div>
        <div>
            Total <div class="salida"><?= $total ?></div>
        </div>
    </body>
</html>
