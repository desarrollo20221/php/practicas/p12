<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            img{
                width: <?= $ancho ?>px;
                height: <?= $alto ?>px;
            }
            .dados{
                width: <?= $ancho * 2 ?>px;
            }
            .total{
                margin: 10px
            }
            .total>span{
                border: 2px solid black;
                padding: 10px;                
            }
            .mayor{
                border: 2px solid black;
                padding: 10px;
            }
        </style>
    </head>
    <body>        
        <?php
        $anho= 150;
        $alto= 150;
        
        $sumaDados = [];

        for ($c = 0; $c < 10; $c++) {
            $dado1 = rand(1, 6);
            $dado2 = rand(1, 6);
            $sumaDados[] = $dado1 + $dado2;        
        ?>
        <div>
            <div class="dados">
                <img src="imgs/<?= $dado1 ?>.svg" alt="dado1"/>
                <img src="imgs/<?= $dado2 ?>.svg" alt="dado2"/>
            </div>
            <div class="total">Total: <span> <?= $sumaDados[$c] ?></span></div>
        </div>
        <?php
        }
        var_dump($sumaDados);              
        ?>
        <div class="mayor">
            La tirada con mayor resultado es de: <?= max($sumaDados) ?>
        </div>        
    </body>
</html>
